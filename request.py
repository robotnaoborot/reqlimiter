import asyncio
import json
from datetime import datetime, timedelta
from typing import Optional, Dict

import aio_pika
import aiohttp
import aioredis
from pydantic import BaseModel, validator

from config import config
from strategies import strategies


class RequestOptions(BaseModel):
    headers: Dict = {}
    payload: Dict = {}
    timeout: int = 30


class Request(BaseModel):
    sign: Optional[str] = None
    method: str = "GET"
    uri: str
    options: RequestOptions = RequestOptions()
    info: Dict = {}
    expires: Optional[int] = None
    proxy: Optional[str] = None

    @validator("method")
    def method_to_lower(cls, v):
        return v.lower()

    async def perform_request(self):
        from worker import worker
        redis = await worker.get_redis()
        strategy_info = await asyncio.gather(
            *[strategy.get_for_request(self) for strategy in strategies]
        )
        delay = max([0] + [delay for name, ident, delay in strategy_info])
        if delay:
            await self.on_wait(delay, strategy_info)
        else:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.request(
                            self.method.lower(),
                            self.uri,
                            headers=self.options.headers,
                            data=self.options.payload,
                            timeout=self.options.timeout,
                            proxy=self.proxy,
                    ) as resp:
                        body = await resp.text()
                        if self.sign:
                            await redis.set(
                                f"tasks:{self.sign}:request", self.json()
                            )

                        if resp.status != 200:
                            await self.on_error("request", resp.status, body)
                        else:
                            await self.on_success(resp.status, body)
            except Exception as e:
                await self.on_error("exception", e.__class__.__name__, str(e))
                raise
        for name, ident, delay in strategy_info:
            if ident:
                await redis.zadd("req:{name}:{ident}", datetime.now().timestamp())

    async def trigger_event(self, event, body):
        if self.sign:
            body["sign"] = self.sign
        from worker import worker
        connection = await worker.get_rabbit()
        channel = await connection.channel()
        exchange = await channel.declare_exchange("events.req")
        await exchange.publish(
            aio_pika.Message(json.dumps(body).encode('utf-8')), routing_key=f"req.events.{event}",
        )

    async def on_error(self, error_type, status, body):
        await self.trigger_event(
            "error", {"type": error_type, "code": status, "message": body}
        )
        from worker import worker
        redis = await worker.get_redis()
        await redis.set(f"tasks:{self.sign}:error", body)

    async def on_success(self, status, body):
        await self.trigger_event("success", {})
        from worker import worker
        redis = await worker.get_redis()
        await redis.set(f"tasks:{self.sign}:response", body)

    async def on_wait(self, delay, strategy_info):
        await self.trigger_event(
            "wait",
            {
                "delay": delay,
                "strategies": [name for name, ident, delay in strategy_info],
            },
        )
        from worker import worker
        redis = await worker.get_redis()
        await redis.zadd(
            f"tasks:wait",
            (datetime.now() + timedelta(seconds=delay)).timestamp(),
            self.sign,
        )


class RequestRPC(Request):
    sign: str
    expires: int

    async def on_error(self, error_type, status, body):
        raise NotImplemented

    async def on_success(self, status, body):
        raise NotImplemented

    async def on_wait(self, delay, strategy_info):
        raise NotImplemented
