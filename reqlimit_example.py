"""
Topvisor API: https://topvisor.com/ru/api/v2/intro/
Ограничения:
- С одного IP не более 5 одновременных запросов
- Для одного User-Id не более 5 одновременных запросов

Ниже пример реального запроса, при запуске будут так же ответы с ошибкой (код 429)

Я.Метрика API: https://yandex.ru/dev/metrika/doc/api2/intro/quotas-docpage/
Одно из ограничений: 
- Количество запросов с одного IP-адреса	30 в секунду

При превышении так же будет 429 ошибка
"""

import aiohttp
import asyncio

#"""
req_count = 20
api_url = "https://api.topvisor.ru/v2/json/get/projects_2/projects"
headers={
	'Authorization': 'ed12dfe9ac7162169f5a',
	'User-Id': '182421',
	'Content-Type': 'application/json'
}
#"""

{
	"sign": "kk",
	"method": "GET",
	"uri": "https://api-metrika.yandex.net/management/v1/counters",
	"options": {
"headers":{
	"Authorization": "OAuth AgAAAAAmbikcAAJfbksuwFjyi0XBrRn7mmxYL7I",
	"Content-Type": "application/json"
}	},
	"expires": 30
}

"""
req_count = 60
api_url = "https://api-metrika.yandex.net/management/v1/counters"
headers={
	'Authorization': 'OAuth AgAAAAAmbikcAAJfbksuwFjyi0XBrRn7mmxYL7I',
	'Content-Type': 'application/json'
}
"""

async def get(url):
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.get(url) as response:
            return response


loop = asyncio.get_event_loop()

coroutines = [get(api_url) for _ in range(req_count)]

results = loop.run_until_complete(asyncio.gather(*coroutines))

print("Results: %s" % results)

"""


{
	"sign": "kk",
	"method": "GET",
	"uri": "https://api-metrika.yandex.net/management/v1/counters",
	"options": {
"headers":{
	"Authorization": "OAuth AgAAAAAmbikcAAJfbksuwFjyi0XBrRn7mmxYL7I",
	"Content-Type": "application/json"
}	},
	"expires": 30
}

"""