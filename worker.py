import asyncio
from datetime import datetime

import aio_pika
import json

import aioredis

from request import Request
from config import config


class Worker:
    rabbitmq = None
    redis = None

    async def get_rabbit(self):
        if not self.rabbitmq:
            self.rabbitmq = await aio_pika.connect_robust(config.rabbitmq_dsn, loop=asyncio.get_event_loop())
        return self.rabbitmq

    async def get_redis(self):
        if not self.redis:
            self.redis = await aioredis.create_redis_pool(config.redis_dsn)
        return self.redis

    async def connect(self):
        rabbitmq = await self.get_rabbit()
        if config.retry_thread:
            loop.create_task(worker.restart_waiting_requests())
        channel = await rabbitmq.channel()
        queue = await channel.declare_queue("methods.req.http.create", durable=True)
        await queue.consume(self.on_create)

    async def on_create(self, message: aio_pika.IncomingMessage):
        with message.process():
            req = json.loads(message.body)
            # loop.create_task(process_request(req))
            req = Request(**req)
            await req.perform_request()

    async def restart_waiting_requests(self):
        redis = await self.get_redis()
        while True:
            timestamp = datetime.now().timestamp()
            for sign in await redis.zrangebyscore("tasks:wait", max=timestamp):
                req = await redis.get(f"tasks:{sign}:request")
                req = Request(**req)
                await req.perform_request()
            await redis.zremrangebyscore("tasks:wait", max=timestamp)
            await asyncio.sleep(config.retry_thread_delay)


worker = Worker()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(worker.connect())

    loop.run_forever()
