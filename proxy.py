import asyncio
import uuid
from aio_pika import connect, IncomingMessage, Message

from config import config


class ProxyRpcClient:
    def __init__(self):
        self.connection = None
        self.channel = None
        self.exchange = None
        self.callback_queue = None
        self.futures = {}
        self.loop = None

    async def connect(self):
        self.loop = asyncio.get_event_loop()
        self.connection = await connect(
            config.rabbitmq_dsn, loop=loop
        )
        self.channel = await self.connection.channel()
        self.callback_queue = await self.channel.declare_queue(exclusive=True)
        self.exchange = await self.channel.declare_exchange("methods.proxy")
        await self.callback_queue.consume(self.on_response)

        return self

    def on_response(self, message: IncomingMessage):
        future = self.futures.pop(message.correlation_id)
        future.set_result(message.body)

    async def call(self):
        correlation_id = str(uuid.uuid4())
        future = self.loop.create_future()

        self.futures[correlation_id] = future

        await self.exchange.publish(
            Message(
                '',
                content_type="text/plain",
                correlation_id=correlation_id,
                reply_to=self.callback_queue.name,
            ),
            routing_key="methods.proxy.get",
        )

        return await future


proxy_rpc = ProxyRpcClient()
