import asyncio
import json
import os
from collections import OrderedDict
from datetime import timedelta, datetime
from enum import Enum
from glob import glob
from hashlib import sha256
from typing import List
from urllib.parse import urlparse, parse_qs

import aio_pika
import aioredis

from pydantic import BaseModel, AnyHttpUrl

from config import config
from proxy import proxy_rpc


def get_external_ip():
    import socket

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    adr = s.getsockname()[0]
    s.close()
    return adr


class StrategyType(Enum):
    ip = "ip"
    param = "param"
    info = "info"


class IntervalNames:
    sec = "seconds"
    min = "minutes"
    hour = "hours"
    day = "days"
    week = "weeks"
    month = "months"


class IntervalEnum(Enum):
    sec = "sec"
    min = "minute"
    hour = "hour"
    day = "day"
    week = "week"
    month = "month"


class ParamType(Enum):
    header = "header"
    get = "get"
    post = "post"
    info = "info"


class StrategyParam(BaseModel):
    type: ParamType
    name: str

    def get(self, req):
        return {
            "header": lambda req: req.options.headers.get,
            "get": lambda req: parse_qs(urlparse(req.uri).query).get,
            "post": lambda req: req.options.payload.get,
            "info": lambda req: req.options.info,
        }[self.type.value](req)(self.name)


class StrategyModel(BaseModel):
    name: str
    type: StrategyType
    domain: str
    params: List[StrategyParam] = None
    limit: int = 5
    interval: IntervalEnum

    async def match(self, req):
        uri = urlparse(req["uri"])
        if self.domain != uri.netloc:
            return
        if self.type in (StrategyType.param.value, StrategyType.info.model):
            params = OrderedDict(
                [(param.name, param.get(req)) for param in self.params]
            )
            if any(v is None for v in params.values()):
                return
            return repr(params)
        elif self.type == StrategyType.ip.value:
            ip = get_external_ip()
            retries = 0
            while self.get_delay(ip):
                if not proxy_rpc.connection:
                    await proxy_rpc.connect()
                ip = await proxy_rpc.call()
                req.proxy = ip
                if retries > 5:
                    raise Exception("Can't get non-used proxy")
            return ip

    async def get_delay(self, ident):
        end = datetime.now()
        start = end - timedelta(**{self.interval: 1})
        first = start
        from worker import worker
        redis=worker.redis
        count = await redis.zcount(
            key=f"req:{self.name}:{ident}",
            min=start.timestamp(),
            max=end.timestamp(),
        )
        if count >= self.limit:
            schedule_at = first + timedelta(**{getattr(IntervalNames, self.interval): 1})
            delay = (schedule_at - datetime.now()).seconds
        else:
            schedule_at = datetime.now()
            delay = 0
        return delay

    async def get_for_request(self, req):
        ident = self.match(req)
        delay = self.get_delay(ident) if ident else 0
        return self.name, ident, delay


def get_strategies():
    return [
        StrategyModel(**json.load(open(f)))
        for f in glob(os.path.join(config.strategies_path, "*.json"))
    ]


strategies = get_strategies()
