from typing import Set, Optional

from pydantic import (
    BaseModel,
    BaseSettings,
    PyObject,
    RedisDsn,
    Field,
    AnyUrl,
)


class AmqpDsn(AnyUrl):
    allowed_schemes = {"amqp"}


class Settings(BaseSettings):
    redis_dsn: RedisDsn = "redis://user:pass@localhost:6379/1"
    rabbitmq_dsn: AmqpDsn = "amqp://"

    retry_thread: bool = True
    retry_thread_delay: int = 1
    workers_count: int = 1
    session_timeout: Optional[int] = None
    strategies_path: str = ""

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


config = Settings()
